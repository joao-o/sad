set terminal png size  800,600
set output "DNLINL.png"
set samples 1000

set xlabel "DAC code"
set ylabel "INL/DNL"
set grid

f(x)=floor(x)*4.696/1024

plot "dnlinl.txt"  lw 2 notitle

set output "DNLINL.png"
set xrange [0:1024]
plot "dnlinl.txt" using 1:2 title 'DNL' w lines, "dnlinl.txt" using 1:3 title 'INL' w lines

set output "DNLINL2.png"
set xrange [0:4096]
plot "dnlnl.txt" using 1:2 title 'DNL' w lines, "dnlnl.txt" using 1:3 title 'INL' w lines
