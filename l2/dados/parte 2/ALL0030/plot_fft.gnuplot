set terminal png
set samples 1000
set style line 2 lc rgb '#FF0000' lt 1 lw 1 pt 2 ps 0.2
set style line 3 lc rgb '#0000FF' lt 1 lw 1 pt 2 ps 0.2

set grid

fsamp=2.5e7

fname=sprintf("%s.dat",file)
out=sprintf("%s.png",file)
set output out

set xrange [0:fsamp/2]
set xlabel "f(Hz)"
set ylabel "|F(w)|"

plot fname using 1:2 notitle ls 2
