set terminal png size  800,600
set output "spi_tf.png"
set samples 1000

set xlabel "DAC code"
set ylabel "U(V)"
set grid

f(x)=floor(x)*4.096/4096

plot "transferfunction.txt"  lw 2 notitle

set output "spi_tf_z.png"
set xrange [100:200]
plot "transferfunction.txt"  lw 2 notitle,f(x) notitle
