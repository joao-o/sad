set terminal png size 800,600
set samples 1000
set style line 2 lc rgb '#FF0000' lt 1 lw 2 pt 2 ps 0.2
set style line 3 lc rgb '#0000FF' lt 1 lw 2 pt 2 ps 0.2

set grid

fsamp=1e6

set output "fft_comp.png"

set xrange [0:fsamp/2]
set xlabel "f(Hz)"
set ylabel "|F(w)|(dB)"

plot "ch1.fft.dat" using 1:2 notitle w lines ls 2,\
   "ch2.fft.dat" using 1:2 notitle  w lines ls 1\

