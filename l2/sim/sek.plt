set terminal png size 800,600
set output "sek_bode.png"
set xlabel "f(Hz)"
set ylabel "vo/vi(dB)"
set grid
set logscale x
set xrange [1e+00:1e+06]
set xrange [1.000000e+00:1.000000e+06]
set mxtics 10
set grid mxtics
unset logscale y 
set yrange [-7.323099e+01:3.486582e+00]
set format y "%g"
set format x "%g"

plot 'sek.data' using 1:2 with lines lw 3 notitle 
