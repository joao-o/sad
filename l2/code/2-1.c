/* 
 * File:   main.c
 * Author: user
 *
 * Created on 17 October 2016, 15:42
 */

#include <libpic30.h>			  //C30 compiler definitions
#include <uart.h>					  //UART (serial port) function and utilities library
#include <timer.h>				  //timer library
#include <stdio.h>
#include <stdlib.h>
#include <adc10.h>

#define FCY ((long) 7372*4)	  //instruction frequency in kHz
//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF);					  //watchdog timer is off
#define RXBSZ 20

unsigned int str_pos = 0;
char rx_buf[RXBSZ];				  //buffer used to store characters from serial port
unsigned int command_avail = 0;
unsigned int porta, i,j=0;

void
intrep_command(void)
{
/*reads and executes commands arriving via serial port*/
    int i;
	rx_buf[str_pos - 1] = 0;
	switch (rx_buf[0]) {
    case 'n': /*escolhe o valor (0-3) a converter no DAC)  */
      porta = atoi(rx_buf+1);
      printf("%d %d %d %d %d\n\r",porta,(porta>>0)&0x0001,(porta>>1)&0x0001,(porta>>2)&0x0001,(porta>>3)&0x0001);
      _LATB0 = porta&0x0001;
      _LATB1 = (porta>>1)&0x0001;
      _LATB2 = (porta>>2)&0x0001;
      _LATB3 = (porta>>3)&0x0001;
      break;
    case 't': /*Determina a frequência de um ciclo onde mostra todos os valores possíveis a converter*/
        i=atoi(rx_buf+1);
        PR3=i;
        break;
    case 'a': /* Liga/desliga o modo "ciclo" */ 
        IEC0bits.T3IE ^=1;
        break;
	default:
	  puts("?\r");
	}
	str_pos = 0;
	command_avail = 0;
}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{
	IFS1bits.U2RXIF = 0;
    _LATF0 ^= 1;
    char c;
	while (U2STAbits.URXDA) {	  // reads bytes from the uart into the buffer
		c = U2RXREG;
		putchar(c);
		rx_buf[str_pos++] = c;
		if (str_pos >= RXBSZ) {	  // rx_buf is a circular buffer
			str_pos = 0;
		}
	}
	if (c == '\r') {				  // check for a newline in buffer
		command_avail = 1;
		putchar('\n');
		putchar('\r');
	}
}

void __attribute__ ((interrupt, auto_psv)) _T3Interrupt(void) /* Executa ciclo (faz incremento do valor a representar e envia esse valor para o DAC) */
{ 
    _T3IF=0;
    j++;
    if(j==16){
        j=0;
    }
    _LATB0 = j&0x0001;
    _LATB1 = (j>>1)&0x0001;
    _LATB2 = (j>>2)&0x0001;
    _LATB3 = (j>>3)&0x0001;
}

int main(void) {
    unsigned int config0, config1;
    ADPCFG = 0xFFFF;                 /* Configuranção de portas série */
    _TRISB0 = 0;
    _TRISB1 = 0;
    _TRISB2 = 0;
    _TRISB3 = 0;
    _TRISB4 = 0;
    
    config0 = T3_ON & T3_GATE_OFF & T3_PS_1_8 & T3_SOURCE_INT;
	OpenTimer3(config0, 30000);
	IEC0bits.T3IE = 0;
    
    //activates the uart in continuous mode (no sleep) and 8bit no parity mode7372*4
	config0 = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
	//activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
	config1 = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
	OpenUART2(config0, config1, 15);	// umode, u2sta, 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1;					  //0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0;		  //disables hardware loopback on UART2. Enable only for tests
	U2MODEbits.STSEL = 0;
	__C30_UART = 2;				  //define UART2 as predefined for use with stdio library, printf etc
	puts("\n\rSerial port ONLINE\r");	//to check if the serial port is working
    
    while (1) {
		if (command_avail)
			intrep_command();      
	}
    
    
    return (EXIT_SUCCESS);
}

