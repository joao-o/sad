#include <stdio.h>
#include <stdlib.h>


int main(int argc,char**argv)
{
	FILE *f=fopen(argv[1],"r");
	char buf[50];
	unsigned char test;

	while(fgets(buf,50,f)!=NULL){
		test=atoi(buf);
		for(int i=0;i<8;i++)
			printf("%d\n",(test>>i)&0x1);
	}
	return 0;
}
