pkg load control signal
tsamp=1/9830.4;

function fftdb(list)
20*log(abs(fft(list)))/log(10)
endfunction

#file="21hzp-dec.dat";
m = textread(file);
#m = m*-1/32768;
pts = length(m);
x = [0:pts-1];
tx = x*tsamp;
fx = x*1/tsamp/pts;
delta=zeros(pts,1);
delta(2)=1;

fc=76;

b = fir1(127,2*fc*tsamp);
d=filter(b,1,delta);
l=filter(b,1,m);

dfft=20*log(abs(fft(d)))/log(10);
lfft=20*log(abs(fft(l)))/log(10);
mfft=20*log(abs(fft(m)))/log(10);

plot(fx,dfft)
#plot(fx,abs(fft(d))
#plot(tx,d)
#plot(tx,l,tx,m)
#plot(tx,l)
#plot(fx,mfft)
#plot(fx,mfft,fx,lfft,fx,dfft)

#fid=fopen("coeffs","w");
#for i=1:length(b)
#	fprintf(fid,"%f\n",b(i));
#endfor
#fclose(fid);
