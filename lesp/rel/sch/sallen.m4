.PS
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
"$V_{out}$"
move right 0.2
dot
resistor(right_ 0.6);llabel(,"$22\mathrm{k}\Omega$");
dot
{
line up 0.4
capacitor(right_ 1.4);llabel(,"$33\mathrm{nF}$");
line down 0.52
dot
}
resistor(right_ 0.5);llabel(,"$22\mathrm{k}\Omega$");
{
dot
capacitor(down_ 0.5);rlabel(,"$33\mathrm{nF}$");
ground(,T);
}
line right 0.3
X1:opamp(right_ 0.6,,,,R) with .In1 at Here
move to X1.Out
line right 0.1
dot
move right 0.3
"$V_{ADC}$"
move to X1.In2
line left 0.1
line down 0.3
line right 0.7 then up to X1.Out




.PE

