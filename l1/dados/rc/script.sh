#!/bin/sh 

for i in l1*dat 
do
	gnuplot -e \
	"file=\"${i%.dat}\";c0=$(echo $i | sed -n 's/.*f\(.*\)-p.*/\1/p')" \
       	cenas.gnuplot
done
cat *txt > megalog.out

cat megalog.out  | grep c: | cut -b3- | sed 's/+-/ /' > col1
cat megalog.out  | grep b: | cut -b3- | sed 's/+-/ /' > col2
cat megalog.out  | grep d: | cut -b3- | sed 's/+-/ /' > col3
paste -d\  col1 col2 col3 > data
maxima -b filter.mac
gnuplot finalplot.gnuplot
/bin/rm col1 col2 col3
