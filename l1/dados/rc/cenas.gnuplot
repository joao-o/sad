set terminal png
set print  sprintf("%s.txt",file)
set fit errorvariables
set samples 1000
set style line 2 lc rgb '#FF0000' lt 1 lw 1 pt 2 ps 0.2
set style line 3 lc rgb '#0000FF' lt 1 lw 1 pt 2 ps 0.2

tsamp=1.0/(7372800.0*4.0/846.0)

f(x)=a0+b0*sin(c0*x*2*pi+d0)
g(x)=a1+b1*sin(c1*x*2*pi+d1)

a0=2.5;  a1=2.5
b0=2.0;  b1=1.9
d0=1.0;  d1=1.0
c1=c0

fname=sprintf("%s.dat",file)
print file

fit f(x) fname using ($1*tsamp):($2*5/1024) via a0,b0,c0,d0
print sprintf("a:%f+-%f\nb:%f+-%f\nc:%f+-%f\nd:%f+-%f",\
	       a0,a0_err,b0,b0_err,c0,c0_err,d0,d0_err)
print sprintf("chi2: %f",FIT_STDFIT)

fit g(x) fname using ($1*tsamp):($3*5/1024) via a1,b1,c1,d1
print sprintf("a:%f+-%f\nb:%f+-%f\nc:%f+-%f\nd:%f+-%f",\
	       a1,a1_err,b1,b1_err,c1,c1_err,d1,d1_err)
print sprintf("chi2: %f\n",FIT_STDFIT)

out=sprintf("%s.png",file)
set output out

plot fname using ($1*tsamp):($2*5/1024) notitle, f(x) notitle,\
     fname using ($1*tsamp):($3*5/1024) notitle, g(x) notitle
