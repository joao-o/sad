set terminal png
set fit errorvariables
set samples 1000
set style line 2 lc rgb '#FF0000' lt 1 lw 1 pt 2 ps 0.2
set style line 3 lc rgb '#0000FF' lt 1 lw 1 pt 2 ps 0.2

set logscale x
set grid

f(x)=20*log(1/sqrt((x/f_c)*(x/f_c)+1))/log(10)
g(x)=atan2(-x,f_c)*180/pi
f_c=4350.0

set xrange [200:20000]
set xlabel "f(Hz)"

set output "mag.png"
set yrange [-15:5]
set ylabel "A(dB)"
plot "toplot.dat" using 1:3:2:4 with xyerrorbars notitle ls 3, f(x) notitle ls 2


set yrange [-90:0]
set output "phase.png"
set ylabel "fase(º)"
plot "toplot.dat" using 1:($5*180/pi):2:($6*180/pi) with xyerrorbars notitle ls 3,\
g(x) notitle ls 2
