set terminal X11
set title "rc"
set xlabel "Hz"
set grid
set logscale x
set xrange [1e+01:1e+06]
set xrange [1.000000e+01:1.000000e+05]
set mxtics 10
set grid mxtics
unset logscale y 
set yrange [-9.422449e+01:7.204031e-01]
#set xtics 1
#set x2tics 1
#set ytics 1
#set y2tics 1
set format y "%g"
set format x "%g"
plot 'plot.data' using 1:2 with lines lw 1 title "(ph(v(2))-ph(v(1)))*180/pi" 
set terminal push
set terminal png
set out 'plot.png'
replot
set term pop
replot
