#include <libpic30.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <adc10.h>

#define FCY ((long) 7372*4)	  /*instruction frequency in kHz*/

/*Configuration bits*/
_FOSC(CSW_FSCM_OFF & XT_PLL16); /*oscilator at 16x PLL*/
_FWDT(WDT_OFF);					  /*watchdog timer is off*/

#define RXBSZ 20                /*variables for serial command reception*/
unsigned int str_pos = 0;
char rx_buf[RXBSZ];				  
unsigned int command_avail = 0;

unsigned int t_lvl    = 512,\ /* variables for the trigger mechanism*/
             t_prev   = 0  ,\
			    t_cur    = 0  ,\
				 t_enable = 0;

unsigned int jitter = 0; /* variables for the jitter mechanism */
int PRj = 0,\
    PRf = 0;

#define D_SZ 512        /* large array to store measured data */
unsigned int dat[D_SZ];
unsigned int dati = D_SZ;

void
intrep_command(void)
/*reads and executes commands arriving via serial port*/
{
	int i;
	rx_buf[str_pos - 1] = 0;  /* the last byte is set to 0 */
									  /* for cstring compatibility */
	switch (rx_buf[0]) {
	case 't':  /* change sampling frequency, f_samp=FCY/PR3, */
		        /* recommend no larger than ~200kHz */
		PRf = atoi(rx_buf + 1);
		PR3 = PRf;
		break;
	case 'a': /* start sampling, will fill the buffer and then stop */
		t_enable = 1;
		break;
	case 'l': /* set level of software trigger */
		t_lvl = atoi(rx_buf + 1);
		break;
	case 'j': /* toggle sampling time jitter */
		jitter ^= 1;
		printf("jitter %d\n\r", jitter);
		break;
	case 'p': /* print samples in buffer */
		puts("PRINTING \r");
		for (i = 0; i < D_SZ; i++)
			printf("%d\n\r", dat[i]);
		puts("DONE PRINTING \r");
		break;
	default:
		puts("?\r");
	}
	str_pos = 0;
	command_avail = 0;
}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
/* uart receprion interrupt here we put recieved characters in buffer
 * do a simple echo back and detect newlines in order to signal the main 
 * loop there are commands to execute */
{
	IFS1bits.U2RXIF = 0;

	char c;
	while (U2STAbits.URXDA) {	  /* reads bytes from the uart into the buffer */
		c = U2RXREG;
		putchar(c);
		rx_buf[str_pos++] = c;
		if (str_pos >= RXBSZ) {	  /* rx_buf is a circular buffer */
			str_pos = 0;
		}
	}
	if (c == '\r') {				 
		command_avail = 1;
		putchar('\n');putchar('\r');
	}
}

void __attribute__ ((interrupt, auto_psv)) _ADCInterrupt(void)
/* adc conversion finished interrupt, here we store the result 
 * in the buffer if required and we also implement the software trigger */
{
	_ADIF = 0;
	if (dati < D_SZ) { 
		/* we only store the conversion result if the buffer is not yet filled 
		 * this way dati also acts a flag to start the sampling process*/
		dat[dati++] = ADCBUF0;
	} else { 
		/* the trigger continiusly checks if the assigned value for its level
		 * is between the previous and the current conversion results 
		 * if it is it starts sampling by setting dati to 0 */
		t_prev = t_cur;
		t_cur = ADCBUF0;
		if (t_enable && t_prev < t_lvl && t_cur > t_lvl) {
			dati = 0;
			t_enable = 0;
		}
	}
}

void __attribute__ ((interrupt, auto_psv)) _T3Interrupt(void)
/* Timer 3 isr, this timer is used to set the sampling frequency 
 * here we also do the necessary calculations for simulating a jitter
 * on the adc sampling period */
{
	_T3IF = 0;
	_LATC14 ^= 1; /* we toggle this pin in order to obtain an accurate reading
						* of the sampling frequency */
	_SAMP = 1;
	if (jitter) {
		/* jitter of +- 10% uniformly distributed */
		PRj = PRf + ((rand() - RAND_MAX / 2) / 5) / (RAND_MAX / PRf);
		PR3 = PRj;
	}
}

int main(void)
{
	unsigned int config0, config1;
	_TRISF0 = 0;
	_TRISC14 = 0;
	_TRISB3 = 1;

	/* Serial port config */
	/* activates the uart in continuous mode (no sleep) 
	 * and 8bit no parity mode7372*4
	 */
	config0 = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
	/* activates interrupt of pin Tx + 
	 * enables Tx + enable Rx interrupt for every char
	 */
	config1 = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
	OpenUART2(config0, config1, 15);	
   /*        umode  , u2sta  , 115200 bps */
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1;
	U2MODEbits.LPBACK = 0;
	U2MODEbits.STSEL = 0;
   /* define UART2 as predefined for use with stdio library, printf etc */
	__C30_UART = 2; 
	puts("\n\rSerial port ONLINE\r");

	/* timer */
	config0 = T3_ON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT;
	OpenTimer3(config0, 1000);
	IEC0bits.T3IE = 1;

	/* adc
	 * refer to the apropriate section of the manual to check what these do
	 */
	ADCON1bits.ADSIDL = 1;
	ADCON1bits.FORM = 0;
	ADCON1bits.SSRC = 7;
	ADCON1bits.SIMSAM = 0;
	ADCON1bits.ASAM = 0;

	ADCON2bits.VCFG = 0;
	ADCON2bits.CSCNA = 0;
	ADCON2bits.CHPS = 0;
	ADCON2bits.SMPI = 0;
	ADCON2bits.BUFM = 0;
	ADCON2bits.ALTS = 0;

	ADCON3bits.SAMC = 10;		  /* sample for 2us */
	ADCON3bits.ADRC = 0;
	ADCON3bits.ADCS = 11;		  /* ~ 200ns target time for tad */

	ADCHSbits.CH0SA = 2;			  /* positive input for channel 0 */
	ADCHSbits.CH0NA = 0;

	ADCON1bits.ADON = 1;

	_ADIE = 1;

	ADPCFG = 0x0000;

	/* main loop */
	while (1) { 
		if (command_avail)
			intrep_command();
	}
	return 0;
}
