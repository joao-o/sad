#include <libpic30.h>			  //C30 compiler definitions
#include <uart.h>					  //UART (serial port) function and utilities library
#include <timer.h>				  //timer library
#include <stdio.h>
#include <stdlib.h>
#include <adc10.h>

#define FCY ((long) 7372*4)	  //instruction frequency in kHz
//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF);					  //watchdog timer is off
#define RXBSZ 20

unsigned int str_pos = 0;
char rx_buf[RXBSZ];				  //buffer used to store characters from serial port
unsigned int command_avail = 0;

unsigned int t_lvl = 512;
unsigned int t_prev = 0;
unsigned int t_cur = 0;
unsigned int t_enable = 0;

unsigned int jitter = 0;
int PRj=0;
int PRf=0;

#define D_SZ 512
unsigned int dat[D_SZ];
unsigned int dati = D_SZ;

//Reads UART commands
void
intrep_command(void)
{
    int i;
	rx_buf[str_pos - 1] = 0;
	switch (rx_buf[0]) {
	case 'r':                         //read
      _SAMP=1;
	  break;
    case 't':
      PRf = atoi(rx_buf+1);
      PR3=PRf;
      break;
    case 'a':
      t_enable=1;
      break;
    case 'l':
      t_lvl=atoi(rx_buf+1);
      break;
    case 'j':
      jitter^=1;
      printf("jitter %d\n\r",jitter);
      break;
    case 'p':
      puts("PRINTING \r");
      for (i=0;i<D_SZ;i++)
          printf("%d\n\r",dat[i]);
      puts("DONE PRINTING \r");
      break;
	default:
	  puts("?\r");
	}
	str_pos = 0;
	command_avail = 0;
}
void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{
	IFS1bits.U2RXIF = 0;
    _LATF0 ^= 1;
    char c;
	while (U2STAbits.URXDA) {	  // reads bytes from the uart into the buffer
		c = U2RXREG;
		putchar(c);
		rx_buf[str_pos++] = c;
		if (str_pos >= RXBSZ) {	  // rx_buf is a circular buffer
			str_pos = 0;
		}
	}
	if (c == '\r') {				  // check for a newline in buffer
		command_avail = 1;
		putchar('\n');
		putchar('\r');
	}
}

void __attribute__((interrupt, auto_psv)) _ADCInterrupt(void)
{
    _ADIF = 0;
    if (dati < D_SZ){
      dat[dati++]=ADCBUF0;
    } else {
      t_prev=t_cur;
      t_cur=ADCBUF0;
      if (t_enable && t_prev < t_lvl && t_cur > t_lvl){
            dati = 0;
            t_enable = 0;
      }
    }
    
}

void __attribute__ ((interrupt, auto_psv)) _T3Interrupt(void)
{
    _T3IF=0;
    _LATC14 ^= 1;
    _SAMP=1;
    if(jitter){
        PRj=PRf+((rand()-RAND_MAX/2)/5)/(RAND_MAX/PRf);
        PR3=PRj;
    }
}

main(void)
{
	unsigned int config0, config1;
	_LATF0 = 1;						  //LED cfg
	_TRISF0 = 0;
    _TRISC14 = 0;
    _TRISB3 = 1;
	ADPCFG = 0x0000;				  /* IC8 is used so we need to ser portB pins as digital */
	/* Serial port config */
    
	//activates the uart in continuous mode (no sleep) and 8bit no parity mode7372*4
	config0 = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
	//activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
	config1 = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
	OpenUART2(config0, config1, 15);	// umode, u2sta, 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1;					  //0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0;		  //disables hardware loopback on UART2. Enable only for tests
	U2MODEbits.STSEL = 0;
	__C30_UART = 2;				  //define UART2 as predefined for use with stdio library, printf etc
	puts("\n\rSerial port ONLINE\r");	//to check if the serial port is working
    
	config0 = T3_ON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT;
	OpenTimer3(config0, 1000);
	IEC0bits.T3IE = 1;
    
     /* adc */
    ADCON1bits.ADSIDL = 1;
    ADCON1bits.FORM = 0;
    ADCON1bits.SSRC = 7;
    ADCON1bits.SIMSAM = 0;
    ADCON1bits.ASAM = 0;
    
    ADCON2bits.VCFG = 0;
    ADCON2bits.CSCNA = 0;
    ADCON2bits.CHPS= 0;
    ADCON2bits.SMPI = 0;
    ADCON2bits.BUFM = 0;
    ADCON2bits.ALTS = 0;
    
    ADCON3bits.SAMC = 10; /* sample for 2�s */
    ADCON3bits.ADRC = 0;
    ADCON3bits.ADCS = 11; /* ~ 200ns target time for tad */
    
    ADCHSbits.CH0SA = 2;  /* positive input for channel 0 */
    ADCHSbits.CH0NA = 0;
    
    ADCON1bits.ADON = 1;
    
    _ADIE = 1;
    
    ADPCFG = 0x0000; // Selects pin A0 as analog input
    /*done adc*/
    
	while (1) {
		if (command_avail)
			intrep_command();
	}
 
    return 0;
}