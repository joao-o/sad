#include <libpic30.h>
#include <uart.h>
#include <timer.h>
#include <stdio.h>
#include <stdlib.h>
#include <incap.h>
#include <outcompare.h>

#define FCY (7372800UL*4UL)
_FOSC(CSW_FSCM_OFF & XT_PLL16);
_FWDT(WDT_OFF);
#define RXBSZ 20

unsigned int str_pos = 0;
char rx_buf[RXBSZ];
unsigned int command_avail = 0;

#define NUM_VALS 512
#define ARMED -1
#define START 0
unsigned int val[NUM_VALS];
int val_i=NUM_VALS;
unsigned long int t=10000;

#define CFG_MASK 0x1000

void dac_write(int val)
/* this function writes the value to the dac using the spi interface
 * with the correct protocol, it selects the 4.096V range
 */
{
	val=(val&0x0FFF)|CFG_MASK;
	_LATB2=0;
	while(SPI1STATbits.SPITBF);
	SPI1BUF=val;
	while(!SPI1STATbits.SPIRBF);
	val=SPI1BUF;
	_LATB2=1;
}

void
intrep_command(void)
/* execute commands */
{
	int j;
	rx_buf[str_pos - 1] = 0;
	switch (rx_buf[0]) {
		case 's':
		    dac_write(atoi(rx_buf + 1));
		    break;
		case 'r':
		    printf("%d\n\r",_RB8);
		    break;
		case 't':
		    t=atol(rx_buf + 1);
		    break;
		case 'f':
			 PR3=atoi(rx_buf + 1);
			 break;
		case 'p':
		    puts("printing!\r");
		    for(j=0;j<NUM_VALS;j++)
		        printf("%u\n\r",vals[j]);
		    break;
		case 'a':
			 val_i=START;
			 break;
		case 't':
			 val_i=ARMED;
			 break;
		case 'l':
		    level=atoi(rx_buf + 1);
		    break;
		default:
	  puts("?\r");
	}
	str_pos = 0;
	command_avail = 0;
}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
/* serial port read routine */
{
	IFS1bits.U2RXIF = 0;
	char c;
	while (U2STAbits.URXDA) {
		c = U2RXREG;
		putchar(c);
		rx_buf[str_pos++] = c;
		if (str_pos >= RXBSZ) {
			str_pos = 0;
		}
	}
	if (c == '\r') {
		command_avail = 1;
		putchar('\n');
		putchar('\r');
	}
}

void __attribute__ ((interrupt, auto_psv)) _T3Interrupt(void)
/* we use timer 3 to sample at a fixed rate */
{
	int test=0,i;
	static int testp;
	_T3IF=0;

	_LATB5=0;			/* set the track&hold to hold */
	__delay32(50);		/* wait for it to settle (~1.5us) */
	for(i=11;i>=0;i--) { /* SAR algorithm */
		test=test+(1<<i); /* generate test value */
		dac_write(test); 
		__delay32(FCY/t); /* wait for the dac to react and for the 
		                   * comparator to settle */
		if(_RB8)          /* sample comparator pin to check if 
								 * test is bigger than input value */
			test-=(1<<i);
	}
	_LATB5=1;			   /* put track&hold back in hold mode */

	if (val_i==ARMED && test>level && testp<level) /* trigger is rising edge */
		val_i=START;

	if (val_i<A_SZ) 
		val[val_i++]=test;

	testp=test;
}

int main(void)
{
	unsigned int config0, config1;
	_TRISB2 = 0;
	_TRISB3 = 0;
	_TRISF6 = 0;
	_TRISF3 = 0;
	_LATB2=1;
	_TRISB8=1;
	_TRISB5=0; /* track & hold control */
	ADPCFG = 0xFFFF; /* analog pins are digital */
	
	/* Serial port config */
	config0 = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
	config1 = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
	OpenUART2(config0, config1, 15);
		     /* umode, u2sta, 115000 bps */
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1;
	U2MODEbits.LPBACK = 0;
	U2MODEbits.STSEL = 0;
	__C30_UART = 2;	
	puts("\n\rSerial port ONLINE\r");

	/* timer config */
	config0 = T3_ON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT;
	OpenTimer3(config0, (unsigned int)(FCY/1000)); 
	/* 1khz sample rate by default */
	_T3IE=0;
	
	/* spi config */
	SPI1CONbits.FRMEN = 0;
	SPI1CONbits.SPIFSD = 0;
	SPI1CONbits.DISSDO = 0;
	SPI1CONbits.MODE16 = 1;
	SPI1CONbits.SMP = 0;
	SPI1CONbits.CKE = 1;
	SPI1CONbits.CKP = 0;
	SPI1CONbits.MSTEN = 1;
	SPI1CONbits.SSEN = 0;
	SPI1CONbits.SPRE = 6;
	SPI1CONbits.PPRE = 2;
	SPI1STATbits.SPIEN = 1;
	
	while (1) {
		if (command_avail)
			intrep_command();
	}
 
	return 0;
}
